<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    // on se connecte a la base
    $cnx = new PDO("mysql:host=localhost;dbname=cci-test", "root", "");
    // permet de preparer la requete sql
    $s = $cnx->prepare("SELECT * FROM etudiant WHERE prenomEtudiant = ?");
    // execute la requete sql
    $s->execute(array('alexis'));
    // on recupere les données / $r = resultat 
    echo "<ul>";
    while ($r = $s->fetch()) {
        echo '<li>' . $r[1] . ' ';
        echo $r['prenomEtudiant'] . '</li>';
    }
    echo "</ul>";
    ?>
</body>

</html>
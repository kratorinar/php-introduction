<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $fruit = 'orange';
    $legume = 'Carotte';
    // function avec une variable en arguments
    function fraise($bam, $boum)
    {
        echo $bam . ' ' . $boum;
    };

    fraise($fruit, $legume);

    if ($fruit == $legume) {
        echo ' ratatouille';
    } else {
        echo ' purée';
    }

    $films = array('tik tik boum', 'spiderman', 'Superman', 'Aquaman', 'IronMan', 'Waterman');

    for ($i = 0; $i < count($films); $i++) {
        echo '<br/>' . $films[$i] . '<br/>';
    }

    class test
    {
        public $a;
        public $b;
        public function affiche($a, $b)
        {
            echo $a . ' ' . $b;
        }
    }

    $moi = new test;
    $moi->affiche('Alexis', 'Fabre');

    ?>
</body>

</html>
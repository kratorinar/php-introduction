<?php 

$imagePath = 'test.png';

// create an image object
$im = imagecreatefrompng($imagePath);

// the path that we want to save our webp file to
$newImagePath = str_replace("png", "webp", $imagePath);

// quality of the new webp image 1-100
// recuce this to decrease the file size
$quality = 50;

// create the webp image
imagewebp($im ,$newImagePath, $quality);

echo '<img src="test.png" style="width: 1000px;"> '; 
echo '<p>Taille png : '.round(filesize('test.png')/1024).' ko</p>';
echo '<img src="test.webp" style="width: 1000px;">';
echo '<p>Taille webp : '.round(filesize('test.webp')/1024).' ko</p>';

?>